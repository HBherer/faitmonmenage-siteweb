/*
// SLIDER
var slideIndex = 0;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);

function currentSlide(n) {
  showSlides(slideIndex = n);

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) { slideIndex = 1 }
  if (n < 1) { slideIndex = slides.length }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) { slideIndex = 1 }
  slides[slideIndex - 1].style.display = "block";
  setTimeout(showSlides, 12000);
}

// showSlides()








showSlides()

document.onload = selec()
document.getElementById('btnSchema').addEventListener('click', showShema)
function showShema() {
  document.getElementById('btnDoc').style.backgroundColor = "#A3B18A"
  document.getElementById('btnDoc').style.color = "#4A4A4A"
  document.getElementById('btnSchema').style.backgroundColor = "#588157"
  document.getElementById('btnSchema').style.color = "#FFFFFF"
  document.getElementById('shemaShow').classList.remove("hideContent")
  document.getElementById('docShow').classList.add("hideContent")
}
document.getElementById('btnDoc').addEventListener('click', showDoc)
function showDoc() {
  document.getElementById('btnSchema').style.backgroundColor = "#A3B18A"
  document.getElementById('btnSchema').style.color = "#4A4A4A"
  document.getElementById('btnDoc').style.backgroundColor = "#588157"
  document.getElementById('btnDoc').style.color = "#FFFFFF"
  document.getElementById('docShow').classList.remove("hideContent")
  document.getElementById('shemaShow').classList.add("hideContent")
}
function selec() {
  document.getElementById('btnSchema').style.backgroundColor = "#588157"
  document.getElementById('btnSchema').style.color = "#FFFFFF"
}
*/





// -------------- [ EVENTS JS ] --------------

var account = {
    id: undefined,
    type: undefined
}

var employees = []

if (document.querySelector("form") != null) {
    const forms = document.querySelectorAll('form')
    for (let i = 0; i < forms.length; i++) {
        forms[i].addEventListener('submit', (e) => {
            e.preventDefault()
            id = e.target.getAttribute('id')
            if(id == 'signin') { App.signin() }
            if(id == 'signup') { App.signup() }
            if(id == 'profil-edit') { App.editProfil() }
        })
    }
}


if (document.querySelector("#data-account") != null) {
    let data = document.querySelector("#data-account")
    account.type = data.getAttribute('data-type')
    account.id = data.getAttribute('data-id')
}


// -------------- [ APPLICATION JS ] --------------


const App = {

    signin: () => {

        const formData = {
            email: document.querySelector('#form-email').value,
            password: document.querySelector('#form-password').value
        }

        const verifData = [
            email = {
                body: formData.email,
                regex: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
                messages: {
                    invalid: "Votre courriel est invalide",
                    empty: "Veuillez saisir votre courriel"
                }
            },
            password = {
                body: formData.password,
                regex: /^[\w@! -\.]{8,}$/,
                messages: {
                    invalid: "Votre mot de passe est invalide",
                    empty: "Veuillez saisir votre mot de passe"
                }
            }
        ]

        App.Validate(verifData, () => {
            Request('/login', 'POST', formData, (e) => {
                let data = JSON.parse(e)
                if (typeof data.error != 'undefined') { return App.showError(data.error) }
                if (typeof data.redirect != 'undefined') { window.location.replace(data.redirect) }
            })
        })
    
    },




    signup: () => {

        const formData = {
            last_name: document.querySelector('#form-lastname').value,
            first_name: document.querySelector('#form-firstname').value,
            email: document.querySelector('#form-email').value,
            password: document.querySelector('#form-password').value,
            postal_code: document.querySelector('#form-postalcode').value,
            phone: document.querySelector('#form-phone').value,
            type: document.querySelector('#form-profil').value
        }

        const verifData = [
            last_name = {
                body: formData.last_name,
                regex: /^[a-zA-Z-]{3,}$/,
                messages: { invalid: "Votre nom est invalide", empty: "Veuillez saisir votre nom" }
            },
            first_name = {
                body: formData.first_name,
                regex: /^[a-zA-Z-]{3,}$/,
                messages: { invalid: "Votre prénom est invalide", empty: "Veuillez saisir votre prénom" }
            },
            email = {
                body: formData.email,
                regex: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
                messages: { invalid: "Votre courriel est invalide", empty: "Veuillez saisir votre courriel" }
            },
            password = {
                body: formData.password,
                regex: /^[\w-@!?&$#]+$/,
                minimum: 8,
                messages: { invalid: "Votre mot de passe est invalide", minimum: "Votre mot de passe doit contenir au moins 8 caractères", empty: "Veuillez saisir votre mot de passe" }
            },
            postal_code = {
                body: formData.postal_code,
                regex: /^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/,
                messages: { invalid: "Votre code postal est invalide", empty: "Veuillez saisir votre code postal" }
            },
            phone = {
                body: formData.phone,
                regex: /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/,
                messages: { invalid: "Votre téléphone est invalide", empty: "Veuillez saisir votre téléphone" }
            },
            type = {
                body: formData.type,
                regex: /^(?:particular|business)$/,
                messages: { invalid: "Votre profil est invalide", empty: "Veuillez choisir un type de profil" }
            }
        ]

        App.Validate(verifData, () => {
            Request('/register', 'POST', formData, (e) => {
                let data = JSON.parse(e)
                if (typeof data.error != 'undefined') { App.showError(data.error) }
                if (typeof data.redirect != 'undefined') { window.location.replace(data.redirect) }
            })
        })

    },





    editProfil: () => {

        const formData = {
            last_name: document.querySelector('#form-lastname').value,
            first_name: document.querySelector('#form-firstname').value,
            email: document.querySelector('#form-email').value,
            postal_code: document.querySelector('#form-postalcode').value,
            phone: document.querySelector('#form-phone').value,
            type: document.querySelector('#form-type').value
        }

        const verifData = [
            last_name = {
                body: formData.last_name,
                regex: /^[a-zA-Z-]{3,}$/,
                messages: { invalid: "Votre nom est invalide", empty: "Veuillez saisir votre nom" }
            },
            first_name = {
                body: formData.first_name,
                regex: /^[a-zA-Z-]{3,}$/,
                messages: { invalid: "Votre prénom est invalide", empty: "Veuillez saisir votre prénom" }
            },
            email = {
                body: formData.email,
                regex: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
                messages: { invalid: "Votre courriel est invalide", empty: "Veuillez saisir votre courriel" }
            },
            postal_code = {
                body: formData.postal_code,
                regex: /^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/,
                messages: { invalid: "Votre code postal est invalide", empty: "Veuillez saisir votre code postal" }
            },
            phone = {
                body: formData.phone,
                regex: /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/,
                messages: { invalid: "Votre téléphone est invalide", empty: "Veuillez saisir votre téléphone" }
            },
            type = {
                body: formData.type,
                regex: /^(?:particular|business)$/,
                messages: { invalid: "Votre profil est invalide", empty: "Veuillez choisir un type de profil" }
            }
        ]

        App.Validate(verifData, () => {
            Request('/profil/edit', 'PUT', formData, (e) => {
                let data = JSON.parse(e)
                if (typeof data.error != 'undefined') { App.showError(data.error) }
                if (typeof data.redirect != 'undefined') { window.location.replace(data.redirect) }
            })
        })
    
    },





    hiring: () => {

        const formData = {
            etat: employees
        }

        Request(`http://127.0.0.1:3001/api/${account.id}`, "PUT", formData, (e) => {
            let data = JSON.parse(e)
            if (data.ok) {
                reloadEmployees()
            }else { showError(data.error) }
        })
            

    },




    Validate: (data, next) => {
        for (let i = 0; i < data.length; i++) {
            if(data[i].body === "") { App.showError(data[i].messages.empty); break }
            if(!data[i].regex.test(data[i].body)){ App.showError(data[i].messages.invalid); break }
            if(i == (data.length - 1)) { next(); break }
        }
    },


    showError: (message) => {
        errorElement = document.querySelector('#error')
        if (errorElement) { errorElement.innerHTML = message }
        console.error(message)
    }
}




const Request = (url, method, data, next) => {
    fetch(url, {
        method: method,
        body: JSON.stringify(data),
        headers: { 'Content-type': 'application/json; charset=UTF-8' }
    })
    .then(response => response.text())
    .then(text => { next(text) })
}



reloadEmployees()

function reloadEmployees() {
    Request('http://127.0.0.1:3001/api/', "GET", undefined, (e) => {
        let data = JSON.parse(e)
        employeesElement = document.querySelector("#employees")
        employeesElement.innerHTML = ""
        
        let h3 = document.createElement("h3")
        h3.innerHTML = "Embaucher employé"
        document.querySelector("#employees").appendChild(h3)

        for (let k in data) {
            let html = `
            <article class="embSizeEmp">
                <div class="embSizeImg">
                    <img class="embProfilImg" src="/images/imageProfile.png" alt="Image employé">
                </div>
                <div class="embInfoEmp">
                    <p>${data[k].nom}</p>
                    <p>${data[k].prenom}</p>
                    <p class="embEtatGreen">${data[k].etat}</p>
                </div>
                <button class="btnEmbaucherEmp" data-button="hiring" data-id="${data[k]._id}">Selectionner</button>
            </article>
            `
            employeesElement.insertAdjacentHTML('beforeend', html )
        }

        const buttons = document.querySelectorAll('button')
        for (let i = 0; i < buttons.length; i++) {
            if (buttons[i].getAttribute("type") != "submit") {
                buttons[i].addEventListener('click', (e) => {
                    e.preventDefault()
                    type = e.target.getAttribute('data-button')
                    id = e.target.getAttribute('data-id')
                    if (type == 'hiring') {
                        id = e.target.getAttribute('data-id')
                        if (!employees.includes(id)) {
                            
                            if (account.type == "particular" && employees.length < 1) {
                                employees.push(id)
                                document.querySelector("[data-type='forfait-submit']").removeAttribute('disabled')
                            }

                            if (account.type == "business" && employees.length < 3) {
                                employees.push(id)
                                document.querySelector("#employees-count").innerHTML = employees.length + "/3"
                                if (employees.length >= 3) {
                                    document.querySelector("button[data-type='forfait-submit']").removeAttribute('disabled')  
                                }
                            }

                        }
                    }

                })
            } else {
                buttons[i].parentElement.addEventListener('submit', (e) => {
                    e.preventDefault()
                    type = e.target.getAttribute('data-type')
                    if (type == "forfait") {
                        App.hiring()
                    }
                })
            }
        }
    })
}