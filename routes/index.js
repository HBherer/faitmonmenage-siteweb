const express = require('express');
const router = express.Router();
const indexController = require('../controllers/index.controller')
const blogController = require('../controllers/blog.controller')
const appController = require('../controllers/app.controller')
var cmsController = require('../controllers/cmsController.js')
const { body, validationResult } = require('express-validator')

//middleware
const { valid } = require('../middlewares/validations')
const { connected, disconnected } = require('../middlewares/auth');



router.get('/', indexController.get.index);
router.get('/presentation', indexController.get.presentation);
router.post('/', indexController.post.addnewsLetter);

/* BLOG */
router.get('/blog', blogController.get.blog)
router.get('/blog/presentation', blogController.get.categoryPresentation)
router.get('/blog/profil', blogController.get.categoryProfil)
router.get('/blog/trucsastuces', blogController.get.categoryTrucsAstuces)
router.get('/article/:id', blogController.get.article)


/* APP */
router.get('/login', disconnected, appController.get.login)
router.get('/register', disconnected, appController.get.register)
router.get('/logout', connected, appController.get.logout)
router.get('/hiring', connected, appController.get.hiring)
router.get('/profil', connected, appController.get.profil)
router.get('/profil/edit', connected, appController.get.profilEdit)
router.get('/profil/delete/:id', connected, cmsController.delUser);

router.post('/login', disconnected, valid.login(), valid.result, appController.post.login)
router.post('/register', disconnected, valid.register(), valid.result, appController.post.register)
router.post('/hiring/edit/:id', connected, appController.post.hiring)

router.put('/profil/edit', connected, valid.profilEdit(), valid.result, appController.put.profilEdit)

module.exports = router;