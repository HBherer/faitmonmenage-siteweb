const { body, validationResult } = require('express-validator')

module.exports.valid = {
  
    login: () => {
        return [
            body('email')
                .notEmpty().withMessage("Veuillez saisir votre courriel")
                .isEmail().withMessage("Courriel Invalide"),
            body('password')
                .notEmpty().withMessage("Veuillez saisir votre mot de passe")
        ]
    },

    register: () => {
        return [
            body('first_name')
                .notEmpty().withMessage("Veuillez saisir votre Prénom")
                .isString().withMessage("Une erreur de s'est produit (prénom - format)")
                .isLength({ max: 20 }).withMessage("Votre prénom est trop long")
                .matches(/^([a-zA-Z-]+)$/).withMessage("Votre Prénom contient des caractères interdits"),
            body('last_name')
                .notEmpty().withMessage("Veuillez saisir votre Nom")
                .isString().withMessage("Une erreur de s'est produit (format)")
                .isLength({ max: 20 }).withMessage("Votre nom est trop long")
                .matches(/^([a-zA-Z-]+)$/).withMessage("Votre mot de passe contient des caractères interdits"),
            body('email')
                .notEmpty().withMessage("Veuillez saisir votre courriel")
                .isEmail().withMessage("Courriel Invalide"),
            body('password')
                .notEmpty().withMessage("Veuillez saisir votre mot de passe")
                .isString().withMessage("Une erreur s'est produit (mot de passe - format)")
                .matches(/^[\w-@!?&$#]+$/).withMessage("Votre mot de passe contient des caractères interdits"),
            body('postal_code')
                .notEmpty().withMessage("Veuillez saisir votre code postal")
                .isString().withMessage("Une erreur s'est produit (code postal - format)")
                .matches(/^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/i).withMessage("Code Postal invalide"),
            body('phone')
                .notEmpty().withMessage("Veuillez saisir votre téléphone")
                .matches(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/).withMessage("Téléphone invalide"),
            body('type')
                .notEmpty().withMessage("Veuillez selectionner votre type de compte")
                .isString().withMessage("Une erreur s'est produit (type - format)")
                .matches(/^(?:particular|business)$/).withMessage("Type de compte invalide")
        ]
    },

    profilEdit: () => {
        return [
            body('first_name')
                .notEmpty().withMessage("Veuillez saisir votre Prénom")
                .isString().withMessage("Une erreur de s'est produit (prénom - format)")
                .isLength({ max: 20 }).withMessage("Votre prénom est trop long")
                .matches(/^([a-zA-Z-]+)$/).withMessage("Prénom Invalide"),
            body('last_name')
                .notEmpty().withMessage("Veuillez saisir votre Nom")
                .isString().withMessage("Une erreur de s'est produit (nom - format)")
                .isLength({ max: 20 }).withMessage("Votre nom est trop long"),
            body('email')
                .notEmpty().withMessage("Veuillez saisir votre courriel")
                .isEmail().withMessage("Courriel Invalide"),
            body('postal_code')
                .notEmpty().withMessage("Veuillez saisir votre code postal")
                .isString().withMessage("Une erreur s'est produit (code postal - format)")
                .matches(/^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/i).withMessage("Code Postal invalide"),
            body('phone')
                .notEmpty().withMessage("Veuillez saisir votre téléphone")
                .matches(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/).withMessage("Téléphone invalide (ex: 123-456-7890)"),
            body('type')
                .notEmpty().withMessage("Veuillez selectionner votre type de compte")
                .isString().withMessage("Une erreur s'est produit (type - format)")
                .matches(/^(?:particular|business)$/).withMessage("Type de compte invalide")
        ]
    },
    
    result: (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) { 
            let error = errors.array({ onlyFirstError: true })
            return res.status(400).json({ error: error[0].msg })
        }
        next()
    }

}

