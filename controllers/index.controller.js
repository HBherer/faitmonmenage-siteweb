const { PresentationModel } = require('./../models/presentationModel')
const { EmployeModel } = require('../models/employeModel');
const { NewletterModel } = require('../models/newsletterModel');

module.exports = {

    get: {
        index: async (req, res, next) => {
            try {
                let document = await EmployeModel.find({etat: "Disponible"}).sort({ _id: -1 }).limit(3);
                res.render('index', { employes: document, title: 'Faitmonmenage | Accueil' });
            } catch (err) {
                console.log(err);
                res.status(500).send("Erreur su serveur");
            }
        },
        presentation: async (req, res, next) => {
            let document = await PresentationModel.find({})
            res.render('presentation', { title: "Faitmonmenage | Présentation", titre: "PRÉSENTATION", titre2: "PRÉSENTATION DE L'ENTREPRISE", presentations: document })
        },
    },
    post: {
        addnewsLetter: async (req, res, next) => {
            try {
                let data = new NewletterModel(req.body);
                data.save(function (err, data) {
                    res.redirect('/')
                })
            } catch (err) {
                console.log(err);
                res.status(500).send("Internal Server error Occured ADD_ARTICLE");
            }
        },
    }

}
