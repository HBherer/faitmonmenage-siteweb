//APPEL FICHIER QUERIES SI BESOINS.

const { EmployeModel } = require('../models/employeModel');

module.exports = {
    
    get: {  
        employe: async (req, res, next) => {
            res.render('employe')
        },
    },
    post: {
        addemploye : async (req, res, next) => {
            let dataemploye = new EmployeModel(req.body);
            dataemploye.save(function (err, dataemploye) {
                if (err) console.error(err);
                res.redirect('/employe')
            })
        },
    }

}