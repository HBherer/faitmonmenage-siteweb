const Mongoose = require("mongoose");
var Schema = Mongoose.Schema;

let ArticleSchema = new Schema({
    title: String,
    img: String,
    author:String,
    date: String,
    category: String,
    content: String,
})

exports.ArticleModel = Mongoose.model("Article", ArticleSchema);