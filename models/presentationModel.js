const Mongoose = require("mongoose");
let Schema = Mongoose.Schema;

const PresentationSchema = new Schema({
    img: String,
    titre: String,
    content: String,
});

exports.PresentationModel = Mongoose.model("Presentation", PresentationSchema);