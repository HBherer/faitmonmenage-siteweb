const mongoose = require('mongoose')

const Schema = mongoose.Schema;

let articlesSchema = new Schema({
    title: { type: String, required: true },
    author: { type: String, required: true },
    create_date: { type: String, required: true },
    category: { type: String, required: true },
    picture_url: { type: String, required: true },
    body: { type: String, required: true }
});

module.exports = mongoose.model("articles", articlesSchema)