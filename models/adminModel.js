const Mongoose = require("mongoose");
let Schema = Mongoose.Schema;

const AdminSchema = new Schema({
    name: String,
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: String
});

exports.AdminModel = Mongoose.model("Admin", AdminSchema);